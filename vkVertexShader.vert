#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inUV;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in uvec4 inColor;

layout(location = 0) out vec4 outColor;

layout(push_constant) uniform matrices 
{
    mat4 modelViewProjection;
} Matrices;

void main()
{
    gl_Position = Matrices.modelViewProjection * vec4(inPosition, 1.0f);
    /* account for vulkans coordinate system conventions */
    // see https://matthewwellings.com/blog/the-new-vulkan-coordinate-system/
    // @TODO move this into C++ as a matrix premultiplication
    gl_Position.y = -gl_Position.y; // y points down (right hand NDC space)
    gl_Position.z = (gl_Position.z + gl_Position.w) / 2.0f;


    outColor = vec4(inColor.x / 255.0f, inColor.y / 255.0f, inColor.z / 255.0f, 1.0f);
    outColor = vec4(inNormal * 0.5f + 0.5f, 1.0f);
}



