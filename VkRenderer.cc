/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

/*===========================================================================*\
*                                                                            *
*   $Revision: 21016 $                                                       *
*   $LastChangedBy: schultz $                                                *
*   $Date: 2015-07-16 16:48:42 +0200 (Do, 16 Jul 2015) $                     *
*                                                                            *
\*===========================================================================*/


#include "VkRenderer.hh"


#include <OpenFlipper/BasePlugin/PluginFunctions.hh>
#include <ACG/GL/ShaderCache.hh>
#include <QFile>
#include <ACG/Math/VectorT.hh>
#include <ACG/Math/Matrix4x4T.hh>
#include <ACG/Math/GLMatrixT.hh>

#include <dlfcn.h>

#include <vector>

#include <QVulkanInstance>
#include <QVulkanWindow>
#include <QOpenGLFunctions_2_1>

#include "glslang/glslang/Public/ShaderLang.h"
#include "glslang/SPIRV/GlslangToSpv.h"
#include "glslang/StandAlone/DirStackFileIncluder.h"

// =================================================


void VkRenderer::initResources() 
{
    m_devFunctions = m_window->vulkanInstance()->deviceFunctions(m_window->device());

    auto pool = m_window->graphicsCommandPool();
    VkCommandBufferAllocateInfo allocInfo;
    memset(&allocInfo, 0x0, sizeof(allocInfo));
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandBufferCount = 1;
    allocInfo.commandPool = pool;
    allocInfo.level = VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_SECONDARY;
    
    
    auto res = m_devFunctions->vkAllocateCommandBuffers(m_window->device(), &allocInfo, &m_cmdBuf);
    assert(res == VkResult::VK_SUCCESS);
    
}
    
   
    
void VkRenderer::startNextFrame() 
{
    
    VkClearValue clearValues[2];
    memset(clearValues, 0x0, sizeof(clearValues));
    clearValues[0].color = {{ m_lastRequestedBackgroundColor.red() / 255.0f, m_lastRequestedBackgroundColor.green() / 255.0f, m_lastRequestedBackgroundColor.blue() / 255.0f, 1.0f }};
    clearValues[1].depthStencil = { 1.0f, 0 };
    
    VkRenderPassBeginInfo rpBeginInfo;
    memset(&rpBeginInfo, 0x0, sizeof(rpBeginInfo));
    rpBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    rpBeginInfo.renderPass = m_window->defaultRenderPass();
    rpBeginInfo.framebuffer = m_window->currentFramebuffer();
            
    const QSize sz = m_window->swapChainImageSize();
    rpBeginInfo.renderArea.extent.width = static_cast<uint32_t>(sz.width());
    rpBeginInfo.renderArea.extent.height = static_cast<uint32_t>(sz.height());
    rpBeginInfo.clearValueCount = 2;
    rpBeginInfo.pClearValues = clearValues;

    /**/
    m_devFunctions->vkCmdBeginRenderPass(m_window->currentCommandBuffer(), &rpBeginInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
    m_devFunctions->vkCmdExecuteCommands(m_window->currentCommandBuffer(), 1, &m_cmdBuf);
    m_devFunctions->vkCmdEndRenderPass(m_window->currentCommandBuffer());
    
    m_window->frameReady();
    m_window->requestUpdate();
}
    


QVulkanWindowRenderer* VulkanWindow::createRenderer()  
{
    return m_renderer;
}




VkRenderer::VkRenderer()
{
    logFile.setFileName("VKRenderer_Log.txt");
    if(logFile.open(QFile::OpenModeFlag::WriteOnly | QFile::OpenModeFlag::Truncate)) {
        logStream.setDevice(&logFile);
    }

}


VkRenderer::~VkRenderer()
{
    logFile.close();

}

void VkRenderer::LogToFile(const QString& _message)
{
    logStream << _message << "\n";
}

void VkRenderer::initializePlugin()
{
      
    auto instance = new QVulkanInstance;
    instance->setLayers(QByteArrayList() << "VK_LAYER_LUNARG_standard_validation");
    instance->setFlags(QVulkanInstance::Flag::NoDebugOutputRedirect);
    if(!instance->create()) {
        log("failed to create vulkan instance");
    } else {
        log("created vulkan instance!");
    }
    auto layers = instance->layers();
    if(!layers.contains("VK_LAYER_LUNARG_standard_validation")) {
        log("no vulkan debug layer support");
    }
    m_window = new VulkanWindow(this);
    m_window->setTitle("Vulkan!!!!");
    m_window->setVulkanInstance(instance);
    m_window->resize(1024, 768);
    m_window->show();
    
}

void VkRenderer::prepareRenderingPipeline(ACG::GLState* _glState, ACG::SceneGraph::DrawModes::DrawMode _drawMode, ACG::SceneGraph::BaseNode* _scenegraphRoot)
{
    using ACG::Vec3f;

    LogToFile("Preparing the rendering pipeline !");

    coreProfile_ = !_glState->compatibilityProfile();

    // grab view transform from glstate
    viewMatrix_ = _glState->modelview();
    camPosWS_ = Vec3f( viewMatrix_(0,3), viewMatrix_(1,3), viewMatrix_(2,3) );
    camDirWS_ = Vec3f( viewMatrix_(0,2), viewMatrix_(1,2), -viewMatrix_(2,2) ); // mind the z flip

  //   printf("pos: %f %f %f\ndir: %f %f %f\n", camPosWS_[0], camPosWS_[1], camPosWS_[2],
  //     camDirWS_[0], camDirWS_[1], camDirWS_[2]);

    // First, all render objects get collected.
    collectRenderObjects(_glState, _drawMode, _scenegraphRoot);

    // ==========================================================
    // Sort renderable objects based on their priority
    // Filter for overlay, lines etc.
    // ==========================================================

    size_t numRenderObjects = 0,
      numOverlayObjects = 0,
      numLineObjects = 0;

    for (std::vector<ACG::RenderObject>::const_iterator it = renderObjects_.begin();
            it != renderObjects_.end(); ++it) {
        //if (!it->overlay && !(it->isDefaultLineObject() && enableLineThicknessGL42_))
        //    numRenderObjects++;
        if (it->overlay)
            numOverlayObjects++;
        //if (enableLineThicknessGL42_ && it->isDefaultLineObject())
        //    numLineObjects++;
    }

    /*
     * Neither clear() nor resize() ever decreases the capacity of
     * a vector. So it has no adverse impact on performance to clear
     * the vectors here.
     */
    sortedObjects_.clear();
    sortedObjects_.reserve(numRenderObjects);

    overlayObjects_.clear();
    overlayObjects_.reserve(numOverlayObjects);

    //lineGL42Objects_.clear();
    //lineGL42Objects_.reserve(numLineObjects);


    sortListObjects_.clear();
    sortListObjects_.reserve(numRenderObjects);

    sortListOverlays_.clear();
    sortListOverlays_.reserve(numOverlayObjects);

    // init sorted objects array
    for (size_t i = 0; i < renderObjects_.size(); ++i)
    {
      if (renderObjects_[i].overlay)
      {
        overlayObjects_.push_back(&renderObjects_[i]);
        sortListOverlays_.push_back(i);
      }
      /*else if (enableLineThicknessGL42_ && numLineObjects && renderObjects_[i].isDefaultLineObject())
      {
        renderObjects_[i].shaderDesc.geometryTemplateFile = "Wireframe/gl42/geometry.tpl";
        renderObjects_[i].shaderDesc.fragmentTemplateFile = "Wireframe/gl42/fragment.tpl";

        // disable color write to fbo, but allow RenderObject to control depth write
        renderObjects_[i].glColorMask(0,0,0,0);

  //      sortedObjects_[sceneObjectOffset++] = &renderObjects_[i];
        lineGL42Objects_.push_back(&renderObjects_[i]);
      }*/
      else
      {
        sortedObjects_.push_back(&renderObjects_[i]);
        sortListObjects_.push_back(i);
      }
    }

    sortRenderObjects();
}




VkBuffer GlToVkCache::TranslateBuffer(GLuint buffer, const BufferDesc& desc, QVulkanDeviceFunctions* vk, QVulkanFunctions* vkFunctions, VkDevice device, VkPhysicalDevice physicalDevice)
{

    char* intermediateBuffer = new char[desc.sizeInBytes];
    
    auto prevBoundBuf = ACG::GLState::getBoundBuf(GL_ARRAY_BUFFER);
    {   /* copy data out of buffer */
        ACG::GLState::bindBuffer(GL_ARRAY_BUFFER, buffer);
        auto data = ACG::GLState::mapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
        memcpy(intermediateBuffer, data, desc.sizeInBytes);    
        ACG::GLState::unmapBuffer(GL_ARRAY_BUFFER);
    }
    ACG::GLState::bindBuffer(GL_ARRAY_BUFFER, prevBoundBuf);
    
    VkBuffer outBuffer;
    {   /* create a new vulkan buffer and upload data */
        VkBufferCreateInfo createInfo = {};
        memset(&createInfo, 0x0, sizeof(createInfo));
        
        createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        createInfo.size = desc.sizeInBytes;
        createInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;    // allow usage as either vertex or index buffer
        createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        
        auto res = vk->vkCreateBuffer(device, &createInfo, nullptr, &outBuffer);
        assert(res == VK_SUCCESS);
        
        VkMemoryRequirements requirements;
        vk->vkGetBufferMemoryRequirements(device, outBuffer, &requirements);
        
        VkMemoryAllocateInfo allocInfo;
        memset(&allocInfo, 0x0, sizeof(allocInfo));
        
        const static auto FindMemoryType = [&](uint32_t typeFilter, VkMemoryPropertyFlags flags) -> uint32_t {
            VkPhysicalDeviceMemoryProperties properties;
            vkFunctions->vkGetPhysicalDeviceMemoryProperties(physicalDevice, &properties);
            for(uint32_t i = 0; i < properties.memoryTypeCount; ++i) {
                if((typeFilter & (1 << i)) && (properties.memoryTypes[i].propertyFlags & flags) == flags) {
                    return i;
                }
            }
            assert(false);
        };
        
        VkDeviceMemory memory;
        
        allocInfo.sType = VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO;
        allocInfo.allocationSize = requirements.size;
        allocInfo.memoryTypeIndex = FindMemoryType(requirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
        res = vk->vkAllocateMemory(device, &allocInfo, nullptr, &memory);
        assert(res == VK_SUCCESS);
        
        vk->vkBindBufferMemory(device, outBuffer, memory, 0);
        
        void* copyDest = nullptr;
        res = vk->vkMapMemory(device, memory, 0, desc.sizeInBytes, 0, &copyDest);
        assert(res == VK_SUCCESS);
        memcpy(copyDest, intermediateBuffer, desc.sizeInBytes);
        vk->vkUnmapMemory(device, memory);
    }
    
    delete[] intermediateBuffer;
    
    _bufferCache[buffer] = outBuffer;
    
    return outBuffer;
}

bool GlToVkCache::GetBuffer(GLuint glBuffer, VkBuffer* outBuffer)
{
    if(_bufferCache.find(glBuffer) == std::end(_bufferCache)) {
        return false;
    }
    *outBuffer = _bufferCache[glBuffer];
    return true;
}


void VkRenderer::renderObject(ACG::RenderObject* _obj, GLSL::Program* _prog, bool _constRenderStates, const std::vector<unsigned int>* _shaderModifiers)
{
    // 
    VkBuffer vertexBuffer;
    if(!stateCache.GetBuffer(_obj->vertexBuffer, &vertexBuffer)) {
        BufferDesc desc;
        GLuint buf = _obj->vertexBuffer;
        auto prevBoundBuf = ACG::GLState::getBoundBuf(GL_ARRAY_BUFFER);
        {   
            ACG::GLState::bindBuffer(GL_ARRAY_BUFFER, buf);
            desc.sizeInBytes = ACG::GLState::getBufferSize(GL_ARRAY_BUFFER);    
        }
        ACG::GLState::bindBuffer(GL_ARRAY_BUFFER, prevBoundBuf);
        
        vertexBuffer = stateCache.TranslateBuffer(_obj->vertexBuffer, desc, m_devFunctions, m_window->vulkanInstance()->functions(), m_window->device(), m_window->physicalDevice());
    }
    VkBuffer indexBuffer;
    if(!stateCache.GetBuffer(_obj->indexBuffer, &indexBuffer)) {
        BufferDesc desc;
        GLuint buf = _obj->indexBuffer;
        auto prevBoundBuf = ACG::GLState::getBoundBuf(GL_ELEMENT_ARRAY_BUFFER);
        {   
            ACG::GLState::bindBuffer(GL_ELEMENT_ARRAY_BUFFER, buf);
            desc.sizeInBytes = ACG::GLState::getBufferSize(GL_ELEMENT_ARRAY_BUFFER);    
        }
        ACG::GLState::bindBuffer(GL_ELEMENT_ARRAY_BUFFER, prevBoundBuf);
        
        indexBuffer = stateCache.TranslateBuffer(_obj->indexBuffer, desc, m_devFunctions, m_window->vulkanInstance()->functions(), m_window->device(), m_window->physicalDevice());
    }
    
    /**/
    // select shader from cache
    GLSL::Program* prog = _prog ? _prog : ACG::ShaderCache::getInstance()->getProgram(&_obj->shaderDesc, _shaderModifiers);
    std::vector<char> vertexSource;
    std::vector<char> fragmentSource;
    if(prog != nullptr) {
        const auto shaderProgram = prog->getProgramId();
        const auto vertexShader = ACG::GLState::getAttachedShader(shaderProgram, GL_VERTEX_SHADER);
        assert(vertexShader != GL_NONE);
        const auto fragmentShader = ACG::GLState::getAttachedShader(shaderProgram, GL_FRAGMENT_SHADER);
        assert(fragmentShader != GL_NONE);
        
        vertexSource.resize(ACG::GLState::getShaderSourceLength(vertexShader));
        fragmentSource.resize(ACG::GLState::getShaderSourceLength(fragmentShader));
        
        ACG::GLState::getShaderSource(vertexShader, vertexSource.data(), vertexSource.size());
        ACG::GLState::getShaderSource(fragmentShader, fragmentSource.data(), fragmentSource.size());

    }
    /**/
    const static auto TranslateVertexDeclaration = [this](const ACG::VertexDeclaration* decl) -> VkPipelineVertexInputStateCreateInfo {
        //
        auto const numElements = decl->getNumElements();
        
        auto attributes = new VkVertexInputAttributeDescription[numElements];
        
        auto* element = decl->getElement(0); 
        for(auto i = 0u; i < numElements; ++i, element = decl->getElement(i)) {
            QString elementDescription("Element #");
            elementDescription.append(QString::number(i)).append(": ");
            elementDescription.append("name : ").append(element->shaderInputName_).append(", ");
            elementDescription.append("offset : ").append(QString::number(element->getByteOffset())).append(", ");
            elementDescription.append("num elements : ").append(QString::number(element->numElements_));
            //log(elementDescription);

            VkFormat floatFormats[] = {  
                VK_FORMAT_UNDEFINED,
                VK_FORMAT_R32_SFLOAT,
                VK_FORMAT_R32G32_SFLOAT,
                VK_FORMAT_R32G32B32_SFLOAT,
                VK_FORMAT_R32G32B32A32_SFLOAT, 
            };
            VkFormat uint8Formats[] = {      
                VK_FORMAT_UNDEFINED,
                VK_FORMAT_R8_UINT, 
                VK_FORMAT_R8G8_UINT, 
                VK_FORMAT_R8G8B8_UINT, 
                VK_FORMAT_R8G8B8A8_UINT
            };
            
            attributes[i].binding = 0;
            attributes[i].offset = element->getByteOffset();
            attributes[i].location = i;
            switch(element->type_) {
                case GL_FLOAT: 
                    attributes[i].format = floatFormats[element->numElements_];
                break;
                case GL_UNSIGNED_BYTE:
                    attributes[i].format = uint8Formats[element->numElements_];
                break;
                default:
                    assert(false);
                break;
            }
            
        }
        
        auto* bindingDesc = new VkVertexInputBindingDescription;
        memset(bindingDesc, 0x0, sizeof(*bindingDesc));
        bindingDesc->binding = 0;
        bindingDesc->stride = decl->getVertexStride();
        bindingDesc->inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        //
        VkPipelineVertexInputStateCreateInfo createInfo;
        memset(&createInfo, 0x0, sizeof(createInfo));
        
        createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        createInfo.vertexBindingDescriptionCount = 1;
        createInfo.pVertexBindingDescriptions = bindingDesc;    // // @NOTE we transfer ownership to the caller here
        createInfo.vertexAttributeDescriptionCount = numElements;
        createInfo.pVertexAttributeDescriptions = attributes;   // @NOTE we transfer ownership of the array to the caller here
           
       
        
        return createInfo;
    };
    
    
    VkPipeline pipeline;
    static VkPipelineLayout pipelineLayout;
    static VkPipelineCache pipelineCache = VK_NULL_HANDLE;
    if(pipelineCache == VK_NULL_HANDLE)
    {
        VkPipelineCacheCreateInfo cacheInfo;
        memset(&cacheInfo, 0x0, sizeof(cacheInfo));
        cacheInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
        
        auto res = m_devFunctions->vkCreatePipelineCache(m_window->device(), &cacheInfo, nullptr, &pipelineCache);
        assert(res == VK_SUCCESS);
        
        VkPushConstantRange constantRange;
        constantRange.stageFlags = VK_PIPELINE_STAGE_VERTEX_SHADER_BIT;
        constantRange.offset = 0;
        constantRange.size = sizeof(float) * 16;
        
        VkPipelineLayoutCreateInfo layoutCreateInfo; 
        memset(&layoutCreateInfo, 0x0, sizeof(layoutCreateInfo));
        layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        layoutCreateInfo.pushConstantRangeCount = 1;
        layoutCreateInfo.pPushConstantRanges = &constantRange; 
        
        res = m_devFunctions->vkCreatePipelineLayout(m_window->device(), &layoutCreateInfo, nullptr, &pipelineLayout);
        assert(res == VK_SUCCESS);
    }
    
    VkExtent2D swapchainExtent = { (uint32_t)m_window->swapChainImageSize().width(), (uint32_t)m_window->swapChainImageSize().height() };
    
    // @TODO make vp and scissor states dynamic
    VkViewport viewport;
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = static_cast<float>(swapchainExtent.width);
    viewport.height = static_cast<float>(swapchainExtent.height);
    viewport.minDepth = 0.0f;
    
    viewport.maxDepth = 1.0f;
    VkRect2D scissor;
    scissor.offset = { 0,  0 };
    scissor.extent = swapchainExtent;
    
    VkPipelineViewportStateCreateInfo viewportState;
    memset(&viewportState, 0x0, sizeof(viewportState));
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;
    
    VkPipelineRasterizationStateCreateInfo rasterState;
    memset(&rasterState, 0x0, sizeof(rasterState));
    rasterState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterState.depthClampEnable = VK_FALSE;
    rasterState.rasterizerDiscardEnable = VK_FALSE;
    rasterState.polygonMode = VK_POLYGON_MODE_FILL; // 
    rasterState.lineWidth = 1.0f;
    rasterState.cullMode = VK_CULL_MODE_BACK_BIT;           // @TODO change after sorting out face winding 
    rasterState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;    // @TODO change after sorting out face winding
    rasterState.depthBiasEnable = VK_FALSE;
    
    VkPipelineMultisampleStateCreateInfo msampleState;
    memset(&msampleState, 0x0, sizeof(msampleState));
    msampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    msampleState.sampleShadingEnable = VK_FALSE;
    msampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    msampleState.minSampleShading = 1.0f;
    msampleState.pSampleMask = nullptr;
    msampleState.alphaToCoverageEnable = VK_FALSE;
    msampleState.alphaToOneEnable = VK_FALSE;
    
    VkPipelineColorBlendAttachmentState colorBlendAttachment;
    memset(&colorBlendAttachment, 0x0, sizeof(colorBlendAttachment));
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_FALSE;
    
    VkPipelineColorBlendStateCreateInfo blendState;
    memset(&blendState, 0x0, sizeof(blendState));
    blendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    blendState.logicOpEnable = VK_FALSE;
    blendState.logicOp = VK_LOGIC_OP_COPY;
    blendState.attachmentCount = 1;
    blendState.pAttachments = &colorBlendAttachment;
    
    
    VkPipelineDepthStencilStateCreateInfo depthState;
    memset(&depthState, 0x0, sizeof(depthState));
    depthState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthState.depthTestEnable = VK_TRUE;
    depthState.depthWriteEnable = VK_TRUE;
    depthState.depthCompareOp = VK_COMPARE_OP_LESS;
    depthState.depthBoundsTestEnable = VK_FALSE;
    depthState.stencilTestEnable = VK_FALSE;
   
    
    VkPipelineVertexInputStateCreateInfo vertexInputInfo = TranslateVertexDeclaration(_obj->vertexDecl);
    
    const static auto TranslateTopology = [](GLenum topology) -> VkPrimitiveTopology {
        switch(topology) {
        case GL_LINES:
            return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
        case GL_LINE_STRIP:
            return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
        case GL_TRIANGLES:
            return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        case GL_TRIANGLE_STRIP:
            return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
        case GL_TRIANGLE_FAN:
            return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
        case GL_POINTS:
            return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
        default:
            return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        }  
    };
    
    VkPipelineInputAssemblyStateCreateInfo inputAssembly;
    memset(&inputAssembly, 0x0, sizeof(inputAssembly));
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = TranslateTopology(_obj->primitiveMode);   // @TODO use underlying primitive type here
    inputAssembly.primitiveRestartEnable = VK_FALSE;
    
    
    VkGraphicsPipelineCreateInfo pipelineCreateInfo;
    memset(&pipelineCreateInfo, 0x0, sizeof(pipelineCreateInfo));
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    
    // @NOTE two level cache is a bit much
    const static auto CreateShaderModule = [](QVulkanDeviceFunctions* vk, VkDevice device, const char* source, size_t sourceLen) -> VkShaderModule {
        static std::unordered_map<std::string, VkShaderModule> cache;
        auto lookupIt = cache.find(std::string(source, sourceLen));
        if(lookupIt != cache.end()) {
            return lookupIt->second;
        }
        
        VkShaderModuleCreateInfo createInfo;
        memset(&createInfo, 0x0, sizeof(createInfo));
        
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = sourceLen;
        createInfo.pCode = reinterpret_cast<const uint32_t*>(source);
        
        VkShaderModule outModule;
        auto res = vk->vkCreateShaderModule(device, &createInfo, nullptr, &outModule);
        assert(res == VK_SUCCESS);
        
        cache[std::string(source, sourceLen)] = outModule;
        
        return outModule;
    };
    
    /*
    const static auto CreateShaderModuleFromGLSL = [](QVulkanDeviceFunctions* vk, VkDevice device, const char* source, size_t sourceLen, EShLanguage type) -> VkShaderModule {
        static std::unordered_map<std::string, VkShaderModule> cache;
        auto lookupIt = cache.find(std::string(source, sourceLen));
        if(lookupIt != cache.end()) {
            return lookupIt->second;
        }
        
        static bool needInit = true;
        if(needInit) {
            glslang::InitializeProcess();
            needInit = false;
        }
        
        
        glslang::TShader shader(type);
        shader.setStrings(&source, 1);
        
        int inputSemanticsVersion = 100;
        glslang::EShTargetClientVersion vulkanClientVersion = glslang::EshTargetClientVersion::EShTargetVulkan_1_0;
        glslang::EShTargetLanguageVersion languageVersion = glslang::EShTargetSpv_1_0;
        
        shader.setEnvInput(glslang::EShSourceGlsl, type, glslang::EShClient::EShClientVulkan, inputSemanticsVersion);
        shader.setEnvClient(glslang::EShClientVulkan, vulkanClientVersion);
        shader.setEnvTarget(glslang::EshTargetSpv, languageVersion);
        
        TBuiltInResource resources;
        EShMessages messages = (EShMessages)(EShMsgSpvRules | EShMsgVulkanRules);
        
        const int defaultVersion = 100;
        
        if(!shader.parse(&resources, defaultVersion, false, messages)) {
            std::cout << shader.getInfoLog() << "\n" << shader.getInfoDebugLog() << std::endl;
        }
        
        glslang::TProgram program;
        program.addShader(&shader);
        if(!program.link(messages)) {
            std::cout << shader.getInfoLog() << "\n" << shader.getInfoDebugLog() << std::endl;
        }
        
        std::vector<uint32_t> code;
        spv::SpvBuildLogger logger;
        glslang::SpvOptions options;
        glslang::GlslangToSpv(*program.getIntermediate(type), code, &logger, &options);
       
        auto module = CreateShaderModule(vk, device, (char*)code.data(), code.size() / sizeof(uint32_t));
        cache[std::string(source, sourceLen)] = module;
        return module;
    };
    */
    
    const static auto CreateShaderModuleFromFile = [](QVulkanDeviceFunctions* vk, VkDevice device, const char* filepath) -> VkShaderModule {
        static std::unordered_map<std::string, VkShaderModule> cache;
        auto lookupIt = cache.find(std::string(filepath));
        if(lookupIt != cache.end()) {
            return lookupIt->second;
        }
        
        QFile codeFile(filepath);
        codeFile.open(QFile::OpenModeFlag::ReadOnly);
        
        std::vector<char> code(codeFile.size());
        codeFile.read(code.data(), codeFile.size());
        codeFile.close();
        
        auto module = CreateShaderModule(vk, device, code.data(), code.size());
        cache[std::string(filepath)] = module;
        
        return module;
    };
    
    VkPipelineShaderStageCreateInfo stageInfo[2];
    memset(&stageInfo, 0x0, sizeof(stageInfo));
    
    {   /* vertex */
        stageInfo[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        stageInfo[0].module = CreateShaderModuleFromFile(m_devFunctions, m_window->device(), "vert.spv");
        //stageInfo[0].module = CreateShaderModuleFromGLSL(m_devFunctions, m_window->device(), vertexSource.data(), vertexSource.size(), EShLanguage::EShLangVertex);
        stageInfo[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
        stageInfo[0].pName = "main";
    }
    {   /* fragment */
        
        stageInfo[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        stageInfo[1].module = CreateShaderModuleFromFile(m_devFunctions, m_window->device(), "frag.spv");
        //stageInfo[0].module = CreateShaderModuleFromGLSL(m_devFunctions, m_window->device(), vertexSource.data(), vertexSource.size(), EShLanguage::EShLangFragment);
        stageInfo[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        stageInfo[1].pName = "main";
    }
    
    pipelineCreateInfo.stageCount = 2;
    pipelineCreateInfo.pStages = stageInfo;   //
    
   
    pipelineCreateInfo.pVertexInputState = &vertexInputInfo;
    pipelineCreateInfo.pInputAssemblyState = &inputAssembly;
    pipelineCreateInfo.pViewportState = &viewportState;
    pipelineCreateInfo.pRasterizationState = &rasterState;
    pipelineCreateInfo.pMultisampleState = &msampleState;
    pipelineCreateInfo.pColorBlendState = &blendState;
    pipelineCreateInfo.pDynamicState = nullptr;
    pipelineCreateInfo.pDepthStencilState = &depthState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = m_window->defaultRenderPass();
    pipelineCreateInfo.subpass = 0;
    pipelineCreateInfo.basePipelineIndex = -1;
    pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
    pipelineCreateInfo.pTessellationState = nullptr;

    auto res = m_devFunctions->vkCreateGraphicsPipelines(m_window->device(), pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);
    assert(res == VK_SUCCESS);
        
    
    m_devFunctions->vkCmdBindPipeline(m_cmdBuf, VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    
    ACG::Matrix4x4f modelView = _obj->modelview;
    ACG::Matrix4x4f projection = _obj->proj;

    const auto modelViewProjection = projection * modelView;
    
    m_devFunctions->vkCmdPushConstants(m_cmdBuf, pipelineLayout, VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, 0, sizeof(float) * 16, modelViewProjection.data());
    
    VkDeviceSize offsets = { 0 };
    m_devFunctions->vkCmdBindVertexBuffers(m_cmdBuf, 0, 1, &vertexBuffer, &offsets);
    m_devFunctions->vkCmdBindIndexBuffer(m_cmdBuf, indexBuffer, _obj->indexOffset, _obj->indexType == GL_UNSIGNED_SHORT ? VK_INDEX_TYPE_UINT16 : VK_INDEX_TYPE_UINT32);   // @TODO handle unsigned byte
    m_devFunctions->vkCmdDrawIndexed(m_cmdBuf, _obj->numIndices, 1, 0, 0, 0);
}


void VkRenderer::finishRenderingPipeline(bool _drawOverlay)
{
    LogToFile("Finishing the rendering pipeline !");
}

void VkRenderer::copyDepthToBackBuffer(GLuint _depthTex, float _scale)
{
    LogToFile("Copying depth to backbuffer!");
}

void VkRenderer::renderDepthMap(int _viewerID, int _width, int _height)
{
    LogToFile("Rendering the depth map");
}

void VkRenderer::render(ACG::GLState* _glState, Viewer::ViewerProperties& _properties)
{
    static int frameIndex = 0;
    LogToFile("------------------");
    LogToFile(QString("Starting frame #%1").arg(frameIndex++));

    // collect renderobjects + prepare OpenGL state
    /**/
    auto res = m_devFunctions->vkResetCommandBuffer(m_cmdBuf, 0);
    assert(res == VkResult::VK_SUCCESS);
    
    VkCommandBufferInheritanceInfo inheritance;
    memset(&inheritance, 0x0, sizeof(inheritance));
    inheritance.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    inheritance.renderPass = m_window->defaultRenderPass();
    inheritance.subpass = 0;
    inheritance.framebuffer = VK_NULL_HANDLE;
    
    
    VkCommandBufferBeginInfo beginInfo = {};
    memset(&beginInfo, 0x0, sizeof(beginInfo));
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
    beginInfo.pInheritanceInfo = &inheritance;
    m_devFunctions->vkBeginCommandBuffer(m_cmdBuf, &beginInfo);
    
    
    
    prepareRenderingPipeline(_glState, _properties.drawMode(), PluginFunctions::getSceneGraphRootNode());

    LogToFile(dumpCurrentRenderObjectsToString(&sortedObjects_[0], true));

    // render every object
    for (int i = 0; i < getNumRenderObjects(); ++i) {
        renderObject( getRenderObject(i));
    }

    // restore common opengl state
    // log window remains hidden otherwise
    finishRenderingPipeline();

    /**/
    m_devFunctions->vkEndCommandBuffer(m_cmdBuf);
    /**/
    m_lastRequestedBackgroundColor = _properties.backgroundQColor();
    //  dumpRenderObjectsToFile("../../dump_ro.txt", &sortedObjects_[0]);
}

