# Open Flipper Vulkan Renderer 

This project contains a very early implementation of a vulkan based renderer for `OpenFlipper`. 
While the existing implementation is very rough, by no means intended to be shipped and depends on a few limiting assumptions (as outlined later in this document), it should provide enough of an idea on how `OpenFlipper` could implement support for Vulkan (and potentially other graphics APIs) to proceed with a more solid solution.   


## Getting Started

Checkout the `vulkan-renderer` branch on the following subprojects:

    * OpenFlipper (*not* the root project) 
    * PluginCollection-Renderers

In `OpenFlipper`, the `vulkan-renderer` branch contains required changes to `ACG`, specifically additions to the `GLState` interface which wraps OpenGL functionality. 
In `PluginCollection-Renderers`, the `vulkan-renderer` branch adds the `Plugin-Render-Vulkan` subproject to the project tree, which contains the renderer plugin as well as `glslang` as a submodule (you might still need to run `git submodule update --init` in the directory containing this document).

*NOTE: It is also required to copy the `.spv` files found in the `Plugin-Render-Vulkan` subproject into the `bin` folder of the OpenFlipper build directory in order to be able to run with the Vulkan renderer.* 

 ### QT version

 Vulkan support at the moment uses the Vulkan wrapper provided by QT. This handles all the linking and loading of entry points required to use Vulkan and provides convenient high level objects to simplify usage of the Vulkan API.
 However, it requires linking against a QT version that has been built with Vulkan support enabled. Prebuilt binaries found in the public QT archives might not satisfy this requirement. When working from a machine at the institute, the centralized QT builds should work though.


## Code Overview

As mentioned, Vulkan support is implemented via QTs Vulkan wrapper. Since an explanation of the Vulkan API is out of the scope of this document, this knowledge is assumed in the following. Please refer to the official Vulkan specification or one of the tutorials available online.

### Vulkan, QT and OpenFlipper
The main flow of a Vulkan application written with the QT Vulkan wrappers is controlled by a `QVulkanWindow`. This class encapsulates a system window setup with a vulkan swapchain and a surface to redner into, as well as all the required per-frame primitives. This includes a command buffer, a framebuffer setup with color- and depth attachments, etc. A default Vulkan render pass is also provided by the interface.
While the `QVulkanWindow` encapsulates most of the Vulkan objects needed for rendering a frame to a window surface with reasonable defaults, the main entry points are implemented by a `QVulkanRenderer`. This interface implements a callback, `startNextFrame`, in which rendering can be performed. The main class of the `Plugin-Render-Vulkan` module, `VkRenderer`, implements this interface. In order to instantiate it, `VulkanWindow` derives from `QVulkanWindow` and overrides the `createRenderer` method to return an instance of `VkRenderer`. 

However, `VkRenderer` also has to implement the `RenderInterface` defined by OpenFlipper in order to be usable as a renderer *in* OpenFlipper. This interface requires its own callback, `render`, that is being called by OpenFlipper at some point during the frame.
This poses the issue of synchronizing two callbacks during the frame - one called by `QVulkanWindow`, one called by OpenFlipper.
The current solution to this problem makes use of secondary command buffers in Vulkan. When OpenFlipper is ready to render a frame and calls `render` on the `VkRenderer` instance, the `QVulkanWindow` is not in a valid state for rendering. Instead, `VkRenderer` records rendering commands into a secondary `VkCommandBuffer` without a render pass scope. This command buffer then contains all draw calls and state blocks for the last frame rendered by OpenFlipper.
When `startNextFrame` is called on `VkRenderer` by the `QVulkanWindow`, the most recently recorded command buffer is submitted into a render pass scope using the windows default render pass and currently active framebuffer.

### Render Object Submission

The main entry point for rendering in `VkRenderer` is `render`. This method is responsible for command buffer setup for the secondary command buffer which records the main scene rendering. At the current time, `prepareRenderingPipeline` and `finishRenderingPipeline` are just stubs that have been taken from `Plugin-Render-ShaderPipeline` more or less verbatim. They contain setup largely independent of Vulkan like object sorting.
The actual meat of the implementation lies within `VkRenderer::renderObject`. This method is responsible for actually recording state changes and draw calls for a given `ACG::RenderObject`.

The challenge in integrating a vulkan renderer with OpenFlipper is that the assumption of OpenGL is deeply tied with the OpenFlipper core at the moment. This is appearent in the format used to submit renderable objects. Each `ACG::RenderObject` carries with it the entire state required to draw it correctly, encoded using OpenGL API types and constants. This includes state blocks, but also geometry buffers, which are created as OpenGL buffer objects outside of the renderer, and shaders, which are retreived from `ACG::ShaderCache` as GLSL shaders.

The current implementation of `VkRenderer` deals with this by mirroring buffers (and shaders, sort of).
This is implemented via `GlToVkCache`. At the moment, this provides methods to translate an OpenGL buffer into a Vulkan buffer object, as well as retreive a buffer if it has already been cached. 
When calling `renderObject`, the renderer first tries to look up the corresponding vertex and index buffers using their OpenGL handle as a key. Any buffer for which a cached mirror isn't available will be translated and can then be used in future render calls for the corresponding object. 
This implies a potentially huge cost whenever an object is encountered for the first time, because the implementation has to download all buffers from whichever memory they currently reside in to RAM - an operation which can be extremely expensive, since it forces the driver to make the buffer available to the CPU, which potentially involves *another* copy from GPU exclusive memory to a shared memory heap, as well as potential synchronization because the buffer might actually still be in use by the GPU (although this is highly unlikely considering at this point in the frame no OpenGL renderer is running), *then* followed by the actual copy into RAM. 
However, considering that this cost is only paid *once* per object, it's an acceptable temporary solution until OpenFlipper introduces an API for render objects that allows them to directly store their data in buffers native to the API used by the current renderer (or even separate code paths in the core for OpenGL and Vulkan based renderers, see notes about future work below).

### Current Limitations

To finish render object submission, besides providing buffers for geometry data, the renderer also has to generate a suitable pipeline state object encapsulating the shaders to be used as well as all state blocks and input assembly information. The latter is derived from the information provided by the `ACG::RenderObject` interface.
State blocks are, at the point of this writing, hardcoded in the renderer implementation. This also applies to shaders. While state blocks are trivial to implement - merely a translation / mapping from OpenGL state enumerations to Vulkan state create infos - shaders pose a greater challenge since they are provided as `GLSL` source code targetting OpenGL. Vulkan however requires shaders to be provided as `SPIR-V` bytecode. Translation of `GLSL` to `SPIR-V` might require a few modifications to the provided `GLSL` strings. At the point of writing, the renderer uses a hardcoded, precompiled set of shaders.
This also means that `VkRenderer` only supports objects with a very specific vertex layout at the moment:

@TODO describe vertex layout

The following sections outline how the current implementation should be improved to approach full support of OpenFlippers rendering features.

## Improvements and Future Work

### Short Term Improvements

*NOTE: these are likely to be applied in the immediate future, but only apply to the _current_ implementation which is likely to completely vanish inside the recommended long term architectural overhaul*

The first steps in improving the current implementation should be 

* Adding support for correct application of render state, i.e. remove hardcoded render state blocks and actually translate from the render object state.
* Adding support for shader translation: This can be achieved by embedding a `GLSL -> SPIR-V` compiler, using `glslang`, for example. First steps in that direction have been taken, however at this point `glslang` is not linked into OpenFlipper correctly and thus is not functional. 
In addition to fixing the linking of `glslang`, to enable correct compilation, the shader source provided by a given render object might need to be modified in order to be compliant with the requirements of compilation to `SPIR-V`. 
* Provide caching mechanisms for the above points, similar to how buffers are being cached at the moment.

With these steps, `VkRenderer` would probably be able to support most of OpenFlippers rendering.
However, a solid implementation that also actually enables the benefits of Vulkan to be reaped, a deeper integration is necessary.

### Long Term Improvements

Long term, the best route to enable proper Vulkan support for OpenFlipper would be to provide an entirely separate code path in the core of the application. This also eliminates the currently slightly awkward handling of the `QVulkanWindow`, which in the current state of the implementation is a dedicated window that doesn't allow input. Interaction happens through the default viewport. A solid implementation would need to actually create a `QVulkanWindow` as main viewport.
In addition to that, an abstraction over GPU resources as well as render pipeline state is required to enable most of OpenFlipper to function without knowledge of the graphics API being used underneath. This is a relatively difficult task, however should be reasonably achievable by considering the following points:

* Enable operation of OpenFlipper in two modes: OpenGL mode and Vulkan mode. 
* Hide resource creation behind an interface with distinct implementations for Vulkan and OpenGL. This could leverage the existing plugin system:
    * Plugins related to rendering, like actual renderer plugins, can indicate which modes they are compatible with. For most plugins this will either be OpenGL or Vulkan. 
    * A new interface is added for plugins providing methods for creation of GPU resources. An implementation for OpenGL and Vulkan is provided. Render objects use this interface to create buffers and other resources.
    * A similar interface is provided for the shader cache (see existing `ACG::ShaderCache`). This is used by renderers at runtime to retrieve usable shader code, either as `GLSL` or `SPIR-V`, from the existing shader description format inside of render objects.

A thing to note here: The explicit support of OpenGL and Vulkan modes in OpenFlippers core is a limitation in that it doesn't make it easier to add more graphics APIs in the future. However, considering OpenFlippers target audience and history, it is highly unlikely that support for DirectX or Metal will be a desired feature in the future (Mac support can be realized on Vulkan via MoltenVK). Hence, this is probably a good tradeoff that enables good integration of both OpenGL and Vulkan in the core, without the added complexity of having to make this core support generic against the used graphics API.
Furthermore, this architecture allows existing plugins to function without too much modification, as well as leverage of existing codepaths in OpenFlipper for the OpenGL implementations of the additional required interfaces.


#### Smaller ToDos

Some more very short term things to improve in the current architecture:

* Sort out ownership over various memory allocations
* Pull local functions into dedicated methods, separate stages out into separate objects maybe

Refer also to @NOTE and @TODO markup in the code itself. 