/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

/*===========================================================================*\
*                                                                            *
*   $Revision: 13374 $                                                       *
*   $LastChangedBy: moebius $                                                *
*   $Date: 2012-01-13 09:38:16 +0100 (Fri, 13 Jan 2012) $                     *
*                                                                            *
\*===========================================================================*/


#pragma once

#include <QObject>

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/RenderInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>

#include <ACG/GL/IRenderer.hh>
#include <QTextStream>


#include <QVulkanFunctions>
#include <QtWidgets>

#include <QVulkanWindowRenderer>

#include <unordered_map>

struct GlPipelineState
{
    GLuint boundShaderProgram;
};

struct BufferDesc
{
    size_t  sizeInBytes = 0;
    char*   rawData = nullptr;
};

class GlToVkCache
{
private:
    
    std::unordered_map<GLuint, VkBuffer> _bufferCache;
    
public:
    
    VkBuffer    TranslateBuffer(GLuint buffer, const BufferDesc& desc, QVulkanDeviceFunctions* vk, QVulkanFunctions* vkFunctions, VkDevice device, VkPhysicalDevice physicalDevice);
    bool        GetBuffer(GLuint glBuffer, VkBuffer* outBuffer);
    VkPipeline  GetPipeline(const GlPipelineState& pipelineState);
};


class VkRenderer;
class VulkanWindow : public QVulkanWindow
{
    VkRenderer* m_renderer;
public:
    VulkanWindow(VkRenderer* renderer) : m_renderer(renderer) {}
    QVulkanWindowRenderer* createRenderer() override;
};

class VkRenderer : public QObject, BaseInterface, RenderInterface, LoggingInterface, ACG::IRenderer, public QVulkanWindowRenderer
{
  Q_OBJECT
    Q_INTERFACES(BaseInterface)
    Q_INTERFACES(RenderInterface)
    Q_INTERFACES(LoggingInterface)

  Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-Render-Vulkan")


signals:
  // LoggingInterface
  void log(Logtype _type, QString _message) override;
  void log(QString _message) override;


public:
  VkRenderer();
  ~VkRenderer() override;

  QString name() override { return (QString("Vulkan Renderer Plugin")); }
  QString description( ) override { return (QString(tr("Vulkan Based Renderer (Test)"))); }

  void startNextFrame() override;
  void initResources() override;
  
private:

    GlToVkCache     stateCache;
  
    QFile           logFile;
    void            LogToFile(const QString& _message);
    QTextStream     logStream;

    VkCommandBuffer m_cmdBuf;
    QVulkanWindow*  m_window = nullptr;
    QVulkanDeviceFunctions* m_devFunctions = nullptr;

    QColor  m_lastRequestedBackgroundColor;
public slots:


  QString version() override { return QString("1.0"); }

  QString renderObjectsInfo(bool _outputShaderInfo) override { return sortedObjects_.empty() ? "" : dumpCurrentRenderObjectsToString(&sortedObjects_[0],_outputShaderInfo); }


  virtual void prepareRenderingPipeline(ACG::GLState* _glState, ACG::SceneGraph::DrawModes::DrawMode _drawMode, ACG::SceneGraph::BaseNode* _scenegraphRoot) override;

  virtual void renderObject(ACG::RenderObject* _obj, GLSL::Program* _prog = nullptr, bool _constRenderStates = false, const std::vector<unsigned int>* _shaderModifiers = nullptr) override;

  virtual void finishRenderingPipeline(bool _drawOverlay = true) override;

  virtual void copyDepthToBackBuffer(GLuint _depthTex, float _scale = 1.0f) override;

  virtual void renderDepthMap(int _viewerID, int _width, int _height) override;


private slots:


    //BaseInterface
    void initializePlugin() override;
    void exit() override {}

    //RenderInterface

    void render(ACG::GLState* _glState, Viewer::ViewerProperties& _properties) override;

    void supportedDrawModes(ACG::SceneGraph::DrawModes::DrawMode& _mode) override { _mode = ACG::SceneGraph::DrawModes::DEFAULT; }
    QString rendererName() override { return "Vulkan Renderer"; }
    QString checkOpenGL() override { return ""; }
};

